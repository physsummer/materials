\section{Потенциал}
\label{sec:potential}

\subsection{Основные определения}
\label{sec:potential_def}

Если заряд $q_0$ поместить во внешнее поле $\vec{E}$ то, как мы видели, на него будет действовать сила $\vec{F} = q_0
\vec{E}$. Если мы хотим этот заряд переместить, то надо будет совершить работу против этой силы (т.е. против поля
$\vec{E}$). Аналогия~--- с напряжённостью поля тяжести $\vec{g}$. 

\begin{wrapfigure}{l}{4cm}
    \begin{tikzpicture}
        \coordinate[dot, label = below:$A$] (A) at (0, 0);
        \coordinate[dot, label = below:$B$] (B) at (3, 2);
        \draw[thick, decorate, decoration = {ticks}] (A) .. controls (1, 0) and (2, 2) .. (B);
        \draw (A) .. controls (1, 0) and (2, 2) .. (B) node[pos = 0.27, inner sep = 0mm](x) {};
        \draw[thick, ->] (x) -- ++(45:0.6cm) node[midway, above, sloped] {$\Delta \vec{l}$};
        \draw[thick, ->] (x) -- ++ (-15:1.5cm) node[right] {$\vec{E}$};
        \draw (x) ++ (13:0.5cm) node[blue] {$\alpha$};
    \end{tikzpicture}
    \vspace{1cm}
\end{wrapfigure}

Пусть заряд перемещается из точки $A$ в точку $B$ по какой-то траектории.

Чтобы найти работу сил при таком перемещении, нужно разбить траекторию на множество маленьких кусочков длины $\Delta l$. На
каждом кусочке работа равна

\begin{equation}
    \label{eq:potential_def_intro_1}
    \Delta A = F \cdot \Delta l \cdot \cos \alpha,
\end{equation}
где $\alpha$~--- угол между касательной к кусочку и $\vec{F}$ (мы будем это для краткости записывать как $\vec{F} \cdot
\Delta \vec{l}$). Полная работа записывается как сумма:

\begin{equation}
    \label{eq:potential_def_intro_2}
    A = \sum\limits_A^B \vec{F} \cdot \Delta \vec{l} = q_0 \sum\limits_A^B \vec{E} \cdot \Delta \vec{l}. 
\end{equation}

Потенциальная энергия системы при совершении такой работы изменилась на
\begin{equation}
    \label{eq:potential_def_intro_3}
    \Delta U_{A \to B} = -A = -q_0 \sum\limits_A^B \vec{E} \cdot \Delta \vec{l}.
\end{equation}

\textbf{Разностью потенциалов} между точками $A$ и $B$ называется отношение этого изменения потенциальной энергии к величине
заряда $q_0$ (\textbf{пробного заряда}):

\begin{equation}
    \label{eq:potential_def}
    \Delta \phi = \phi_B - \phi_A = \frac{\Delta U_{A \to B}}{q_0} = -\sum\limits_A^B \vec{E} \cdot \Delta \vec{l}.
\end{equation}

Потенциальную энергию обычно отсчитывают от какой-то точки $O$, которую условно принимают за $0$. Действуя таким образом,
можем ввести понятие \textbf{потенциала} $\phi_A$ в точке $A$:

\begin{equation}
    \label{eq:potential_def_2}
    \phi_A = \frac{\Delta U_{O \to A}}{q_0} = - \sum\limits_O^A \vec{E} \cdot \Delta \vec{l}.
\end{equation}
Отметим, что потенциал $\phi$ и разность потенциалов $\Delta \phi$~--- характеристика электрического поля, а не пробного
заряда $q_0$.



\subsection{Пример: потенциал в однородном поле}
\label{sec:potential_homogeneous_field}

\begin{wrapfigure}{l}{3cm}
    \vspace{-0.25cm}
    \begin{tikzpicture}
        \foreach \x in {0, 1, 2}{
            \draw[very thick, marrow] (\x, 0) -- ++(0, -2cm);
        };
        \coordinate[dot, label = left:$A$] (A) at (1, -0.5);
        \coordinate[dot, label = left:$B$] (B) at (1, -1.5);
        \draw[blue, thick, <->] ($(A) + (1.3, 0)$) -- ($(B) + (1.3, 0)$) node[right, midway] {$d$};
        \draw[dashed, blue] (A) -- ++(1.3, 0);
        \draw[dashed, blue] (B) -- ++(1.3, 0);
    \end{tikzpicture}
\end{wrapfigure}

В качестве первого упраженения посчитаем разность потенциалов между двумя точками в однородном поле. Заряд перемещается из
точки $A$ в точку $B$ как показано на рисунке, вдоль силовой линии поля. Тогда

\begin{equation}
    \label{eq:potential_homogeneous_1}
    \phi_B - \phi_A = -\sum \vec{E} \cdot \Delta \vec{l} = -\sum E \cdot \Delta l \cdot \cos 0 = -E \sum \Delta l = -E
    \cdot d.
\end{equation}
Заметим, что $\phi_B < \phi_A$, т.к. $E d < 0$. Силовые линии всегда смотрят в сторону уменьшения потенциала.

Усложним задачу: пусть теперь наш пробный заряд перемещается из точки $A$ в точку $B$ не вдоль силовой линии, а по какому-то
произвольному пути (см. рисунок).

\begin{wrapfigure}{l}{4cm}
    \vspace{-0.25cm}
    \begin{tikzpicture}
        \foreach \x in {0, 0.7, 1.4, 2.1, 2.8}{
            \draw[very thick, marrow] (\x, 0) -- ++(0, -2.5cm);
        };
        \coordinate[dot, label = above:$A$] (A) at (0.25, -0.5);
        \coordinate[dot, label = above:$B$] (B) at (2.5, -2);
        \draw[very thick, red, marrow] (A) to[out = 270,in = 170] (B);
    \end{tikzpicture}
    \vspace{0.5cm}
\end{wrapfigure}

Разобьём эту траекторию на много маленьких кусочков; разность потенциалов по-прежнему равна

\begin{equation}
    \label{eq:potential_homogeneous_2}
    \phi_B - \phi_A = -\sum \vec{E} \cdot \Delta \vec{l} = -\sum E \cdot \Delta l \cdot \cos \alpha.  
\end{equation}

Электрическое поле везде одинаково, поэтому его можно вынести за знак суммы. Все оставшиеся слагаемые имеют вид $\Delta l
\cos \alpha$. Это~--- проекция длины кусочка $\Delta l$ на вертикальное направление, $\Delta h$. Получается такая сумма:

\begin{equation}
    \label{eq:potential_homogeneous_3}
    \phi_B - \phi_A = -E \sum \Delta h = -E \cdot d. 
\end{equation}
Получился такой же ответ, как и в \eqref{eq:potential_homogeneous_1}, не зависящий от конкретного пути.

Мы видим, что все точки, лежащие в плоскости, перпендикулярной $\vec{E}$, имеют одинаковый потенциал относительно $A$. Такая
поверхность (поверхность одинакового потенциала) называется \textbf{эквипотенциальной}. 

\subsection{Пример: потенциал точечного заряда}
\label{sec:potential_point_charge}

\begin{wrapfigure}{l}{4cm}
    \vspace{-0.5cm}
    \begin{tikzpicture}
        \coordinate[dot, label = $Q$] (Q) at (0, 0);
        \coordinate[dot, label = $q$, label = below:{\small 1}] (q1) at (1, 0);
        \coordinate[dot, label = $q$, label = below:{\small 2}] (q2) at (3, 0);
        \draw[thick] (Q) -- (q1) node[midway, below] {$r_1$};
        \draw[thick, dashed] (q1) -- (q2);
        \draw[thick, <->] ($(Q) + (0, -0.7)$) -- ($(q2) + (0, -0.7)$) node[midway, below] {$r_2$};
  \end{tikzpicture}
\end{wrapfigure}

Подсчитаем разность потенциалов для ещё одной простой конфигурации поля~--- точечного заряда. Пусть имеется заряд $Q$. На
расстоянии $r_1$ от него находится пробный заряд $q$. Заряд перемещают по прямой на расстояние $r_2$ от заряда $Q$. Какова
будет разность потенциалов между точками 2 и 1? 

По определению, $\Delta \phi$ равно

\begin{equation}
    \label{eq:potential_point_charge_1}
    \Delta \phi = \phi_2 - \phi_{1} = -\sum\limits_{r_1}^{r_2} E \cdot \Delta l = - k Q \sum\limits_{r_1}^{r_2} \frac{1}{l^2}
    \Delta l.
\end{equation}
Направление напряжённости поля везде совпадает с направлением перемещения, поэтому вместо скалярного произведения можно брать
обычное.

Посмотрим, как выглядит эта сумма на графике. Выделим среди всех кусочков отрезок, например, от $l_{16}$ до
$l_{17}$. Слагаемое, соответствующее этому кусочку, имеет вид

\begin{equation}
    \label{eq:potential_point_charge_2}
    \frac{1}{l_{16}^2} \cdot \Delta l = \frac{1}{l_{16}^2} \left( l_{17} - l_{16} \right).
\end{equation}

Видно, что это слагаемое соответствует площади прямоугольника, заштрихованного красным на графике, а вся сумма, стало быть,
соответствует площади под графиком функции $1 / l^2$ в пределах $1 / r_1$ до $1 / r_2$. 

\begin{figure}[ht]
    \centering
    \begin{tikzpicture}[
        /pgfplots/axis labels at tip/.style = {
            xlabel style = {
                at = {
                    (current axis.right of origin)
                },
                yshift = -2 ex,
                anchor = west,
                fill = white
            },
            ylabel style = {
                at = {
                    (current axis.above origin)
                },
                yshift=1.75ex,
                anchor=center
            }
        }]
        \begin{axis}[
            xmin = 0.1,
            xmax = 0.5,
            ymin = 0,
            ymax = 60,
            x = 30cm, y = 0.15cm,
            axis x line = bottom,
            axis y line = middle,
            minor tick num = 3,
            axis labels at tip,
            xlabel = {$l$},
            ylabel = {$1 / l^2$},
            xticklabel = \empty,
            yticklabel = \empty,
            extra x ticks = {0.2, 0.25, 0.275, 0.4},
            extra y ticks = {16},
            extra x tick labels = {$r_1$, { $l_{16}$}, { $l_{17}$}, $r_2$},
            extra y tick labels = {{ $\frac{1}{l_{16}^2}$}},
            grid = both]
            \draw[fill = red!20] (axis cs:0.25, 0) rectangle (axis cs:0.275, 16);
            \addplot[very thick, red, mark = none, samples = 50, domain = 0.05:0.5] {1 / x^2};
            \draw [thick, dashed, red] (axis cs:0.2, 0) -- (axis cs:0.2, 25) -| (0, 0);
            \draw [thick, dashed, red] (axis cs:0.4, 0) -- (axis cs:0.4, 6.25) -| (0, 0);
            \draw[->] (axis cs:0.3, 20) node[above] {$\sim \Delta l^2$} to[out = 270, in = 0] (axis cs:0.265, 15);
            \draw[->] (axis cs:0.3, 50) node[right] {$\frac{1}{l_{16}^2} (l_{17} - l_{16})$} to[out = 180, in = 180]
            (axis cs:0.26, 10);
        \end{axis}
    \end{tikzpicture}
    \caption{Вычисление площади под графиком $1 / l^2$}
    \label{fig:potential_point_charge_2}
\end{figure}

Будем считать вместо площади этого прямоугольника площадь трапеции~--- ошибка составит около $\Delta l^2$, то есть,
пренебрежимо малую величину (количество таких треугольников $\sim \left( \Delta l \right)^{-1}$, значит, суммарная ошибка от
всех них $\sim \Delta l$~--- может быть сделана сколь угодно малой). Итак, предлагается вместо
\eqref{eq:potential_point_charge_2} написать

\begin{equation}
    \label{eq:potential_point_charge_3}
    \frac{1 / l_{16}^2 + 1 / l_{17}^2}{2} \left( l_{17} - l_{16} \right).
\end{equation}
Но суммировать такие слагаемые опять сложно. Применим ещё один трюк: учтём, что в случае близких чисел их среднее
арифметическое примерно равно их среднему геометрическому. Иными словами, вместо \eqref{eq:potential_point_charge_3} напишем

\begin{equation}
    \label{eq:potential_point_charge_4}
    \sqrt{\frac{1}{l_{16}^2 l_{17}^2}} \left( l_{17} - l_{16} \right) = \frac{1}{l_{16}} - \frac{1}{l_{17}}.
\end{equation}
Теперь надо просуммировать все такие слагаемые:

\begin{equation}
    \label{eq:potential_point_charge_5}
    \sum\limits_{r_1}^{r_2} \frac{1}{l^2} \Delta l = \frac{1}{r_1} - \frac{1}{l_1} + \frac{1}{l_1} - \frac{1}{l_2} + \ldots +
    \frac{1}{l_N} - \frac{1}{r_2}.
\end{equation}
Видно, что почти всё сократится, а останется
\begin{equation}
    \label{eq:potential_point_charge_6}
    \Delta \phi = \phi_2 - \phi_1 = - k Q \left( \frac{1}{r_1} - \frac{1}{r_2} \right) = k \frac{Q}{r_2} - k \frac{Q}{r_1}.
\end{equation}
Заодно мы подсчитали площадь под графиком $1 / l^2$~--- она оказалась равна $1 / r_1 - 1 / r_2$. 

Установим такое правило: потенциал на бесконечности (когда $r \to \infty$) равен нулю. Тогда можно ввести понятие потенциала
в конкретной точке, удалённой от заряда на расстояние $r$. Из формулы \eqref{eq:potential_point_charge_6} получается, что

\begin{equation}
    \label{eq:potential_point_charge_7}
    \phi (r) = k \frac{Q}{r}. 
\end{equation}
Это выражение мы будем называть \textbf{потенциалом точечного заряда}.

Заметим, что для потенциала тоже действует принцип суперпозиции. Если имеется несколько зарядов $Q_1, Q_2, \ldots$,
расположенных на расстояниях $r_1, r_2, \ldots$ от точки наблюдения, то потенциал в ней равен

\begin{equation}
    \label{eq:potential_point_charge_9}
    \phi = \sum_i k \frac{Q_i}{r_i}. 
\end{equation}

Наконец, отметим ещё один факт. В отличие от напряжённости поля $\vec{E}$ потенциал является скаляром, т.е. имеет всего одну
компоненту. Это иногда облегчает его вычисление.


\subsection{Потенциальная энергия в поле точечного заряда}
\label{sec:potential_energy_point_charge}

В предыдущем разделе мы вычислили разность потенциалов при перемещении пробного заряда $q$ в поле другого заряда $Q$. Теперь
зададимся вопросом: чему равна потенциальная энергия такой системы? 

Ответить на этот вопрос легко, если учесть определение потенциала \eqref{eq:potential_def}:

\begin{equation}
    \label{eq:potential_energy_point_charge}
    U = q \Delta \phi = k \frac{q Q}{r}. 
\end{equation}

При одноимённых зарядах ($q Q > 0$) потенциальная энергия поля положительна и убывает при разведении зарядов. 

Заметим, что потенциал и потенциальная энергия~--- совсем разные вещи. Потенциальная энергия $U$ показывает, какую работу
нужно затратить, чтобы собрать данную систему из отдельных частей (т.е. чтобы заряды $q$ и $Q$ из бесконечности привести на
расстояние $r$ между ними). Потенциал $\phi (r)$, в свою очередь, является функцией положения в пространстве.

Иными словами, потенциал~--- это характеристика только лишь поля, независимая от пробного заряда; потенциальная энергия
же~--- характеристика системы зарядов и поля между ними.

\clearpage