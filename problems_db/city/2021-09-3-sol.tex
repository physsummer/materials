\begin{wrapfigure}[6]{r}{4cm}
\begin{center}
\vspace{-.5cm}
\begin{tikzpicture}[scale=1.5]
	\def\r{.7}
	\fill[pattern=water] (-1,.8) -| (1,1.5) decorate[water edge]
	{-- (-1,1.5)} -- cycle;
	\draw[water edge] (1,1.5) -- (-1,1.5);
	\filldraw[thick, fill=white] (0,{1.5+(\r/2)}) ++(\r,0) arc (0:-180:\r);
	\draw[->] (0,{1.5+(\r/2)}) --++(-150:\r) node[pos=.5,above left=-4pt] {$r$};
	\draw[<->] (.85,1.5) -- ++(0,{\r/2}) node[pos=.5, right] {$r/2$};
	\fill[black!20] (0,{1.5+(\r/2)}) ++(-30:\r) arc (-30:-150:\r) -- cycle;
	\node at (0,{1.5-(\r/4)}) {$V$};
	\draw[thick] (0,{1.5+(\r/2)}) ++(\r,0) arc (0:-180:\r);
\end{tikzpicture}
\end{center}
\end{wrapfigure}
Когда полусфера плавает на поверхности, действующая на неё сила тяжести равна
силе Архимеда:
\begin{equation}
	m g = \rho g V,
\end{equation}
где $V$~--- объём погруженной части (шарового сегмента высоты $r/2$). Объём
\begin{equation}
	V = \frac{m}{\rho}
\end{equation}
пригодится нам в дальнейшем.

Рассмотрим деформированную полусферу под водой. Силу давления воды на неё можно
найти из следующего классического соображения: если бы фигура такой же формы,
как эта деформированная полусфера и воздух в ней, была окружена водой со всех
сторон, то сила Архимеда, действующая на неё, складывалась бы из сил давления
воды на верхнюю и нижнюю части. Верхняя часть состоит из частей сфер, а
нижняя~--- круг. Обозначая силы давления на них за $F$ и $F'$ соответственно,
имеем
\begin{equation}
	F_\text{А} = F' - F.
\end{equation}
Силу давления на нижнюю часть легко найти. На этой глубине это просто
\begin{equation}
	F' = p S = \rho g h \pi r^2.
\end{equation}

\begin{wrapfigure}[6]{r}{4cm}
\begin{center}
\vspace{-.7cm}
\begin{tikzpicture}[scale=1.5]
	\def\r{.7}
	\fill[pattern=water] (-1.1,0) -| (1,1.3) decorate[water edge]
	{-- (-1.1,1.3)} -- cycle;
	\draw[ultra thick, white] (-1,0) -- ++(0,1.3);
	\draw[<->] (-1,0) -- ++(0,1.3) node[pos=.5, left] {$h$};
	\draw[water edge] (1,1.3) -- (-1.1,1.3);
	\draw[<->] (.85,0) -- ++(0,{\r/2}) node[pos=.5, right] {$r/2$};
	\draw[ultra thick, white] (0,.02) ++(\r,0) arc (0:180:\r);
	\draw[thick, dashed] (0,.02) ++(\r,0) arc (0:180:\r);
	\fill[black, opacity=.2] (0,.02) ++(30:\r) coordinate (A) arc (30:150:\r)
	coordinate (B) arc (-150:-30:\r);
	\draw[thick, dashed] (A) -- (B);
	\node at (0,{3*\r/4}) {$V$};
	\node at (0,{\r/4}) {$V$};
	\filldraw[thick,fill=white] (0,.02) ++(\r,0) arc (0:30:\r) arc (-30:-150:\r)
	arc (150:180:\r);
	\draw[platform] (1,0) -- (-1.1,0);
\end{tikzpicture}
\end{center}
\end{wrapfigure}
Чтобы найти силу Архимеда, нужно определить объём деформированной сферы с
воздухом. Понятно, что касание выгнутой части и пола возможно только если высота
этой части равна $r/2$. Тогда нужный нам объём~--- это просто объём полусферы,
из которого вычли $2V$. Таким образом,
\begin{equation}
	F_\text{А} = \rho g \left( \tfrac{2}{3} \pi r^3 - 2 V \right)
	= g \left( \tfrac{2}{3} \pi \rho r^3 - 2 m \right).
\end{equation}
В результате, искомая сила
\begin{equation}
	F = F' - F_\text{А} = \rho g h \pi r^2
	- g \left( \tfrac{2}{3} \pi \rho r^3 - 2 m \right)
	= \pi \rho g r^2 \left( h - \tfrac{2}{3} r \right) + 2 m g.
\end{equation}
\answer{Сила давления воды на деформированную полусферу равна
$\pi \rho g r^2 \left( h - \tfrac{2}{3} r \right) + 2 m g$.}
\medskip

\emph{Комментарий:} объём шарового сегмента $V$ можно найти по формуле
\begin{equation}
	V = \frac{\pi H^2}{3} \left( 3 r - H \right),
\end{equation}
где $H$~--- высота сегмента. Для высоты $r/2$ получаем $V = \tfrac{5}{24} \pi
r^3$, что даёт ответ $F = \pi \rho g r^2 \left( h - \tfrac{r}{4} \right)$.
Нетрудно заметить, что для данной фигуры силу давления можно найти через среднее
давление (давление на глубине $h - r/4$), однако этот факт совершенно неочевиден
и не принимается без доказательства.
