\input{../../../main.tex}

\DeclareMathOperator\artanh{artanh}

\teachermode

\begin{document}

\setphysstyle{ЛФШ 2021}{Редукция размерности}{10 класс}

Ниже изложено теоретическое введение к эксперименту о резиновом мячике,
отскакивающем от пола. Все материалы принимаются однородными и изотропными, а
деформации~--- малыми.

\subsection*{Базовые понятия}%

Рассмотрим продольную деформацию цилиндрического образца высоты $x$ и площадью
$S$. \emph{Модуль Юнга} $E$ связывает относительное удлинение $\varepsilon =
\Delta x / x$ с механическим напряжением $\sigma = F / S$ линейным образом:
$\sigma = E \varepsilon$.
\medskip

\task{Определите размерность модуля Юнга.
\answer{Размерность давления ($M / L T^2$)}}
\task{Найдите коэффициент жёсткости цилиндрического жгута радиуса $r$ и длины
$\ell$, изготовленного из резины с модулем Юнга $E$.
\answer{$k = \pi r^2 E / \ell$}}
\medskip

Пусть образец в виде кругового цилиндра высоты $x$ и диаметра $d$ растянули на
$\Delta x$, в результате чего его диаметр сократился на $\Delta d$. Величину
$\nu = \frac{\Delta d / d}{\Delta x / x}$ называют \emph{коэффициентом Пуассона}
материала.
\medskip

\task{Найдите коэффициент Пуассона для материала, объём которого не меняется при
деформации.
\answer{$1/2$}}


\subsection*{Постановка задачи}%

\begin{wrapfigure}[4]{r}{5cm}
\begin{center}
\vspace{-.9cm}
\begin{tikzpicture}[scale=.9]
	\draw[very thick] (0,0) arc (-90:-60:1) coordinate (a);
	\draw[very thick] (0,0) arc (-90:-120:1) coordinate (b);
	\draw[very thick] (a) arc (-50:230:.78);
	\draw[very thick] (a) arc (120:90:3) -- ++(.3,0) coordinate (h) -- ++(.1,0);
	\draw[very thick] (b) arc (60:90:3);
	\draw (0,0) -- (2.4,0);
	\draw[<->] (h) -- ($(0,0)!(h)!(1,0)$) node[pos=.5,right] {$h$};
	\draw (a) -- ++(0,.4) coordinate (aa) -- ++(0,.1);
	\draw (b) -- ++(0,.4) coordinate (bb) -- ++(0,.1);
	\draw[<->] (aa) -- (bb) node[pos=.5,above] {$D$};
\end{tikzpicture}
\end{center}
\end{wrapfigure}
Шар радиуса $R$ из материала с модулем Юнга $E_1$ и коэффициентом Пуассона
$\nu_1$ вдавливают с силой $F$ в полупространство с модулем Юнга $E_2$ и
коэффициентом Пуассона $\nu_2$. Определить \emph{глубину проникновения} $h$ и
\emph{диаметр области контакта} $D$ (см. рисунок).

В настоящей формулировке эта задача слишком сложна, поэтому приступим к её
упрощению.


\subsection*{Переход к эффективному модулю Юнга}

От контакта двух упругих тел $E_1, \nu_1$ и $E_2, \nu_2$ можно перейти к
контакту жёсткого $E = \infty$ и упругого $E^*, \nu^* = 0$, где
\begin{equation*}
	\frac{1}{E^*} = \frac{1 - \nu_1^2}{E_1} + \frac{1 - \nu_2^2}{E_2}.
\end{equation*}
Таким образом, возможны два эквивалентных варианта упрощения задачи:

\begin{center}
\begin{tikzpicture}[scale=.95]
	\def\r{.9}
	\draw[very thick] (0,0) arc (-90:-60:\r) coordinate (a);
	\draw[very thick] (0,0) arc (-90:-120:\r) coordinate (b);
	\draw[very thick] (a) arc (-50:230:.7);
	\node at (0,.6) {\small $E_1, \nu_1$};
	\draw[very thick] (a) arc (120:90:3);
	\draw[very thick] (b) arc (60:90:3);
	\node at (-1.5,.1) {\small $E_2, \nu_2$};
	\draw[very thick, ->] (-2.3,.7) -- ++(-.45,0);
	\begin{scope}[shift={(-5,0)}]
		\draw[very thick] (0,0) arc (-90:270:.7);
		\node at (0,.7) {\small $E = \infty$};
		\draw[very thick] (0,0) arc (-90:-60:.7) arc (120:90:3);
		\draw[very thick] (0,0) arc (-90:-120:.7) arc (60:90:3);
		\node at (-1.5,.1) {\small $E^*$};
	\end{scope}
	\draw[very thick, ->] (2.3,.7) -- ++(.45,0);
	\begin{scope}[shift={(5,0)}]
		\def\ang{-45}
		\path (0,0) arc (-90:\ang:.7) coordinate (c);
		\draw[very thick, dotted] (c) arc (\ang:{-180-\ang}:.7);
		\draw[very thick] (c) arc (\ang:{180-\ang}:.7);
		\node at (0,.7) {\small $E^*$};
		\draw[very thick] (-2.1,.2) -- (2,.2);
		\node at (-1.5,-.1) {\small $E = \infty$};
		\draw (0,0) -- (2,0);
		\draw[<->] (1.9,0) -- ++(0,-.2) -- ++(0,.6) node[pos=.5,right] {$h$} --
		(1.9,.2);
	\end{scope}
\end{tikzpicture}
\end{center}

Для следующего шага решения мы воспользуемся левым вариантом. В реальности же
наблюдается скорее правый вариант, поэтому для экспериментального определения
$h$ мы будем считать, что шар деформируется путём сминания сферического
сегмента.
\medskip

\task{Получите $E^*$ для контакта резинового мячика $E_{\text{рез.}},
\nu_{\text{рез.}} = 0.5$ и пола, который много жёстче резины.
\answer{$E^* \approx 1.33 E_{\text{рез.}}$}}

\subsection*{Редукция размерности}

Удивительно, но сложную трёхмерную задачу о деформации упругого полупространства
жёстким телом вращения (иногда называемую задачей Буссинеска) всегда можно
свести к простой одномерной задаче о деформации полуплоскости из пружин жёстким
плоским профилем. Такое понижение размерности производится в два шага.

\subsubsection*{Замена профиля}

\begin{wrapfigure}[3]{r}{5.6cm}
\begin{center}
\vspace{-.8cm}
\begin{tikzpicture}
	\draw[->] (0,-.2) -- (0,1.7) node[pos=.9, left] {$f(r)$};
	\draw[->] (0,0) -- (1.2,0) node[pos=.9, below] {$r$};
	\draw[thick] (-1,1) arc (180:360:1) arc (0:360:1 and .2);
	\draw[very thick, ->] (1.3,.7) -- ++(.4,0);
	\begin{scope}[shift={(3,0)}]
		\draw[->] (0,-.2) -- (0,1.7) node[pos=.9, left] {$g(x)$};
		\draw[->] (-1.2,0) -- (1.2,0) node[pos=.9, below] {$x$};
		\draw[thick] (-1,1.2) parabola[bend at end] (0,0) parabola (1,1.2);
	\end{scope}
\end{tikzpicture}
\end{center}
\end{wrapfigure}
Сперва нужно профилю тела вращения $f(r)$ сопоставить плоский профиль $g(x)$ по
формуле
\begin{equation*}
	g(x) = |x| \int\limits_{0}^{|x|} \frac{f'(r)}{\sqrt{x^2 - r^2}} \dd{r}.
\end{equation*}

\task{Опрделите функцию $f(r)$, которой задаётся профиль нижней половины сферы
радиуса $R$.
\answer{$f(r) = R - \sqrt{R^2 - r^2}$}}
\task{Найдите $g(x)$ для профиля из задачи 5.\\
\emph{Эта задача достаточно сложна, и мы не будем пользоваться её ответом в
дальнейшем.}
\answer{$g(x) = x \artanh{(x / R)}$}}
\task{Запишите упрощённый вид для $f(r)$ из задачи 5 при $r \ll R$.\\
\emph{Подсказка:} $(1 + \varepsilon)^n \approx 1 + n \varepsilon$ для
$\varepsilon \ll 1$.
\answer{$f(r) \approx r^2 / 2 R$}}
\task{Найдите $g(x)$ для профиля из задачи 7.
\answer{$g(x) = x^2 / R$}}

\subsubsection*{Замена упругого основания}

\begin{wrapfigure}[9]{r}{4.5cm}
\begin{center}
\vspace{-.9cm}
\begin{tikzpicture}
	\begin{scope}[scale=.9]
		\draw[very thick] (0,0) arc (-90:-60:1) coordinate (a);
		\draw[very thick] (0,0) arc (-90:-120:1) coordinate (b);
		\draw[very thick] (a) arc (-50:230:.78);
		\draw[very thick] (a) arc (120:90:3) -- ++(.1,0) coordinate (h) --
		++(.1,0);
		\draw[very thick] (b) arc (60:90:3);
		\draw (0,0) -- (2.2,0);
		\draw[<->] (h) -- ($(0,0)!(h)!(1,0)$) node[pos=.5,right] {$h$};
		\draw (a) -- ++(0,.4) coordinate (aa) -- ++(0,.1);
		\draw (b) -- ++(0,.4) coordinate (bb) -- ++(0,.1);
		\draw[<->] (aa) -- (bb) node[pos=.5,above] {$D$};
	\end{scope}
	\draw[very thick, ->] (0,-.3) -- ++(0,-.5);
	\begin{scope}[shift={(0,-2.6)}]
		\draw[very thick] (-1.5,1.7) parabola[bend at end] (0,.3) parabola
		(1.5,1.7);
		\draw[platform] (2,0) -- (-2,0);
		\foreach \x in {-1.9,-1.7,...,-.7} {
			\draw[snake=coil, segment length=1.16mm, segment amplitude=.7mm]
			(\x,0) -- ++(0,.6);
		}
		\foreach \x in {1.5,1.3,...,.7} {
			\draw[snake=coil, segment length=1.16mm, segment amplitude=.7mm]
			(\x,0) -- ++(0,.6);
		}
		\draw[snake=coil, segment length=.8mm, segment amplitude=.7mm]
			(.5,0) -- ++(0,.45) (-.5,0) -- ++(0,.45);
		\draw[snake=coil, segment length=.58mm, segment amplitude=.7mm]
			(.3,0) -- ++(0,.34) (-.3,0) -- ++(0,.34);
		\draw[snake=coil, segment length=.46mm, segment amplitude=.7mm]
			(.1,0) -- ++(0,.3) (-.1,0) -- ++(0,.3);
		\draw (0,.3) -- (1.8,.3);
		\draw (-.7,.6) -- (1.8,.6);
		\draw[<->] (1.7,.3) -- ++(0,-.2) -- ++(0,.7) node[pos=.5,right] {$h$} --
		(1.7,.6);
		\draw (.7,.6) -- ++(0,.4) coordinate (a) -- ++(0,.1);
		\draw (-.7,.6) -- ++(0,.4) coordinate (b) -- ++(0,.1);
		\draw[<->] (a) -- (b) node[pos=.5,above] {$D$};
	\end{scope}
\end{tikzpicture}
\end{center}
\end{wrapfigure}
Затем нужно упругому полупространству с модулем Юнга $E^*$ сопоставить
полуплоскость из независимых параллельных пружин жёсткостью $k$, расположенных
на расстоянии $d$ друг от друга, где $k / d = E^*$. Теперь рассмотрим
вдавливание в такую подложку жёсткого профиля, выбранного ранее. Поразительно,
но в этой задаче вдавливающая сила, глубина проникновения и диаметр области
контакта связаны ровно так же, как и в исходной трёхмерной задаче.

Приступим к решению этой одномерной задачи. Зафиксируем отношение $k / d = E^*$,
где $d \ll R$, а вдавливаемый профиль $g(x)$ возьмём из задачи 8.
\medskip

\task{Найдите зависимость диаметра области контакта $D$ от глубины проникновения
$h$.
\answer{$D(h) = 2 \sqrt{R h}$}}
\task{Найдите зависимость вдавливающей силы $F$ от глубины проникновения $h$.
\answer{$F(h) = \frac{4}{3} E^* \sqrt{R} h^{3/2}$}}
\task{Найдите зависимость работы $A$, которую необходимо совершить при
вдавливании профиля на глубину $h$, от этой глубины.
\answer{$A(h) = \frac{8}{15} E^* \sqrt{R} h^{5/2}$}}
\task{В последних двух задачах перейдите от независимого переменного $h$ к $D$.
\answer{$F(D) = \frac{E^*}{6 R} D^3$. $A(D) = \frac{E^*}{60 R^2} D^5$}}


\end{document}
