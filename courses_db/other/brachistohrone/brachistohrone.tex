% Игорь Шендерович, апрель 2023
\input{../../../main.tex}

\parindent=5mm

\begin{document}

\begin{center}
  \LARGE{\textbf{Принцип наименьшего времени и наискорейший спуск}}
\end{center}

\begin{abstract}
Это записки к лекции про принцип наименьшего времени, прочитанной
в апреле 2023 в ФМЛ 30 в рамках дня естественных наук. Формулируется
принцип Ферма, из него получается известный школьникам закон
Снеллиуса. Затем, на этой основе, выписывается уравнение брахистохроны
--- кривой наискорейшего спуска. Интересно, что для её вывода не нужно
ничего варьировать (как в обычном формальном подходе), а
дифференциальное уравнение на траекторию получается быстро и понятно
из физических принципов. 
\end{abstract}

\section{Закон Снеллиуса}
\label{sec:snellius}

\subsection{Принцип Ферма}
\label{sec:fermat}

\begin{wrapfigure}{r}{6cm}\hfill
  \begin{tikzpicture}[thick]
    \draw (0,0) -- (0,4);
    \draw[->] (0,2) -- (5,2)  node[above] {$x$};
    \draw[fill=black] (0,3) coordinate (A) circle (2pt) node[above
    right] {($0,L_1$)};
    \draw[fill=black] (4,0.7) coordinate (B) circle (2pt) node[below] {$(d,-L_2)$};
    \coordinate (C) at (2.5,2);
    \draw (C) node[above right] {$(x,0)$};
    \draw (A) -- (C) -- (B);
    \draw[dashed] ($(C)+(0,-1)$) coordinate (C2) -- (C) -- (B) pic
    [pic text = $\theta_2$, draw=blue, solid, angle radius=5mm, angle
    eccentricity=1.75] {angle = C2--C--B};
    \draw[dashed] ($(C)+(0,1)$) coordinate (C1) -- (C) -- (A) pic
    [pic text = $\theta_1$, draw=blue, solid, angle radius=5mm, angle
    eccentricity=1.75] {angle = C1--C--A};   
  \end{tikzpicture}
\end{wrapfigure}

Обсудим сначала фольклорную задачу. Линия раздела между песком и морем
представляет собой прямую. На расстоянии $L_1$ от берега, на песке,
находится спасатель. На расстоянии $L_2$ от берега, в море, тонет
человек. Спасатель умеет передвигаться по песку со скоростью $v_1$, в
воде он плывёт со скоростью $v_2$. Расстояние между спасателем и
тонущим вдоль берега равно $d$. Как должен двигаться спасатель, чтобы
успеть к тонущему за наименьшее время? 

Эта задача решается введением подходящих координат. Действительно, на
песке спасатель тратит время $t_1 = \sqrt{L_1^2 + x^2}/v_1$, в море
$t_2 = \sqrt{(d-x)^2 + L_2^2}/v_2$. Суммарно затраченное время равно
\begin{equation}
  \label{eq:snellius-time}
  T(x) = t_1 + t_2 = \frac{\sqrt{L_1^2 + x^2}}{v_1} +
  \frac{\sqrt{(d-x)^2 + L_2^2}}{v_2}. 
\end{equation}
Минимальное время соответствует положению $x$ такому, что
\begin{equation}
  \label{eq:snellius-time-derivative}
  \frac{dT}{dx} = \frac{x}{v_1\sqrt{L_1^2 + x^2}} -
  \frac{d-x}{v_2\sqrt{(d-x)^2 + L_2^2}} = 0. 
\end{equation}
Прежде чем решать это уравнение относительно $x$, дадим ему
геометрическую интерпретацию. Действительно, отношение $x/\sqrt{L_1^2
  + x^2} = \sin \theta_1$, а $(d-x)/\sqrt{(d-x)^2 + L_2^2} = \sin
\theta_2$. Таким образом, условие минимальности времени записывается в
виде

\begin{equation}
  \label{eq:snellius-law-v}
  \frac{\sin \theta_1}{v_1} = \frac{\sin \theta_2}{v_2}.
\end{equation}
Понятно, что эта задача имеет оптическую интерпретацию. Пусть скорость
света в вакууме равна $c$, скорость света в среде 1 равна $v_1$, в
среде 2 равна $v_2$, тогда показатели преломления в этих средах равны
$n_1 = v_1/c$, $n_2 = v_2/c$, и наше условие
\eqref{eq:snellius-law-v} записывается в виде

\begin{equation}
  \label{eq:snellius-law-n}
  n_1 \sin \theta_1 = n_2 \sin \theta_2. 
\end{equation}
Это --- знаменитый \textbf{закон Снеллиуса}. Мы получили его из
требования минимальности времения для луча, который должен попасть из
одной точки в другую, преломившись на границе раздела двух сред. Этот
принцип называется \textbf{принципом Ферма}. 

\subsection{Слоистая среда}
\label{sec:layers}

Теперь рассмотрим среду не с одной границей раздела, а с
несколькими. Пусть все границы раздела являются плоскостями,
параллельными друг другу. В каждой среде показатель преломления равен
$n_i$, тогда мы можем написать
\begin{equation}
  \label{eq:snellius-layers}
  n_1 \sin \theta_1 = n_2 \sin \theta_2 = \ldots n_k \sin \theta_k,
\end{equation}
или, иными словами, $n_i \sin \theta_i = \const$. Произведение из
показателя преломления на синус угла падения сохраняется вдоль всей
траектории луча, которая даёт наименьшее время прохождения сквозь
среду. Если сделать <<непрерывный предел>>, и сказать, что угол
падения и показатель преломления будут зависеть от некоторой
непрерывной координаты $y$ (ось направлена по нормали к границе
раздела), то получится
\begin{equation}
  \label{eq:snellius-cont}
  n(y) \sin \theta(y) = \const \quad \mathrm{или} \quad \frac{\sin
    \theta(y)}{v(y)} = \const. 
\end{equation}

\section{Брахистохрона}
\label{sec:brachistochrone}

\subsection{Уравнение траектории}
\label{sec:brachistochrone-equation}

\begin{wrapfigure}{r}{4cm}\hfill
  \begin{tikzpicture}[thick]
    \draw[->] (0,0) -- (3,0) node[above] {\small $x$};
    \draw[->] (0,0) -- (0,-3) node[right] {\small $y$}; 
    \draw[->, blue] (3,-0.5) -- (3,-1) node[midway,right] {$\vec{g}$};
    \draw[fill=black] (0,0) coordinate (A) circle (2pt) node[above]
    {$A$};
    \draw[fill=black] (2.5,-2.5) coordinate (B) circle (2pt) node[right]
    {$B$};
    \draw[dashed] (A) -- (B);
    \draw (A) to[out=-70,in=180] (B);
  \end{tikzpicture}
\end{wrapfigure}

Теперь попробуем решить механическую задачу о
\textbf{брахистохроне}. Пусть имеются две фиксированные точки $A$ и
$B$. В форме какой кривой следует изогнуть проволоку, зафиксированную в
этих точках, чтобы тяжёлая бусинка, скользящая по ней, потратила бы
наименьшее время из $A$ в $B$? Трения нет. Будем для упрощения всех
расчётов считать, что координаты точки $A$ (0,0), $B$ $(L,L)$,
начальная скорость в точке $A$ равна 0. 

Наивный ответ, первым приходящий в голову --- это просто прямая из $A$
в $B$. Скорость в точке $B$ равна $\sqrt{2gL}$ (по закону сохранения
энергии), при этом ускорение на всей траектории остаётся
постоянным. Тогда время перемещения равно длине пути, поделённому на
среднюю скорость движения:
\begin{equation}
  \label{eq:linear-time}
  T = \frac{L\sqrt{2}}{(v_A+v_B)/2} = \frac{2L\sqrt{2}}{\sqrt{2gL}}= 2
  \sqrt{\frac{L}{g}}. 
\end{equation}

Но на самом деле есть траектория, доставляющая бусинку из $A$ в $B$ за
меньшее время. Чтобы это увидеть, разделим всю нашу среду от $A$ до
$B$ на <<слои>>.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}[thick,
    tangent/.style={
        decoration={
            markings,% switch on markings
            mark=
                at position #1
                with
                { \coordinate (tangent
                  point-\pgfkeysvalueof{/pgf/decoration/mark
  info/sequence number}) at (0pt,0pt);
                  \coordinate (tangent unit
                  vector-\pgfkeysvalueof{/pgf/decoration/mark
                    info/sequence number}) at (1,0pt);
                  \coordinate (tangent orthogonal unit
                  vector-\pgfkeysvalueof{/pgf/decoration/mark
                    info/sequence number}) at (0pt,1); 
                }
              },
              postaction=decorate
            },
            use tangent/.style={
              shift=(tangent point-#1),
              x=(tangent unit vector-#1),
              y=(tangent orthogonal unit vector-#1)
            },
            use tangent/.default=1
            % https://tex.stackexchange.com/questions/25928/how-to-draw-tangent-line-of-an-arbitrary-point-on-a-path-in-tikz 
            ]
    \draw[->] (0,0) -- (4.5,0) node[right] {\small $x$};
    \draw[->] (0,0) -- (0,-4.5) node[left] {\small $y$};
    \draw[fill=black] (0,0) coordinate (A) circle (2pt) node[above]
    {$A$};
    \draw[fill=black] (4,-4) coordinate (B) circle (2pt) node[right]
    {$B$};
    \draw[tangent=0.7] (A) to[out=-70,in=170] coordinate[pos=0.7] (C) (B);
    \fill[blue] (C) circle (2pt);
    \draw[line width=0.3pt,dashed] (C) -- ($(A)!(C)!(4.5,0)$) coordinate (x);
    \draw[line width=0.3pt,dashed] (C) -- ($(A)!(C)!(0,-4.5)$) coordinate (y);
    \draw[line width=0.5pt, blue, use tangent] (-2,0) coordinate (t1)
    -- (2,0) coordinate (t2);
    \pic [pic text = $\theta$, draw=blue, solid, angle radius=5mm, angle
    eccentricity=1.5] {angle = x--C--t1};       
  \end{tikzpicture}
  \caption{К выводу уравнению брахистохроны.}
  \label{fig:brachistochrone}
\end{figure}

Вдоль <<слоёв>> непрерывно меняется скорость бусинки и угол, под которым
она пересекает <<границу раздела>>. Угол между траекторией и нормалью
--- это угол между касательной к траектории в данной точке и
нормалью $\theta$ (см. рис. \ref{fig:brachistochrone}). В соответствии
с уравнением \eqref{eq:snellius-cont}
\begin{equation}
  \label{eq:br-01}
  \frac{\sin \theta(y)}{v(y)} = \frac{\sin \theta (y)}{\sqrt{2gy}} =
  \const. 
\end{equation}

Вычислим $\sin \theta(y)$. Для этого заметим, что $\sin \theta = \cos
(\pi/2-\theta)$. Но $\pi/2-\theta$~---~это угол, под которым
касательная пересекает ось $Ox$. При этом $\tg (\pi/2-\theta) =
y'(x)$, где $y(x)$~---~уравнение нашей кривой. Отсюда получаем, что
\begin{equation}
  \label{eq:br-02}
  \sin \theta = \cos (\pi/2-\theta) = \frac{1}{\sqrt{1+\tg^2
      (\pi/2-\theta)}} = \frac{1}{\sqrt{1+y'^2(x)}}.
\end{equation}
С учётом этого, наше условие \eqref{eq:br-01} превращается в
дифференциальное уравнение на $y(x)$:
\begin{equation}
  \label{eq:br-03}
  \sqrt{1+y'^2(x)} \sqrt{2gy}=\const \quad \mathrm{или} \quad y
  (1+y'^2) = C. 
\end{equation}

\subsection{Циклоида и время спуска}
\label{sec:cycloid}

Попробуем теперь решить уравнение \eqref{eq:br-03}. Разделяя
переменные, получаем
\begin{equation}
  \label{eq:cycloid-01}
  dy \sqrt{\frac{y}{C-y}} = dx. 
\end{equation}
Чтобы решить это уравнение, нужно взять интеграл от левой
части. Сделаем замену переменной $y = C \sin^2 \phi$, тогда $dy = 2C
\sin \phi \cos \phi$, и
\begin{equation}
  \label{eq:cycloid-02}
  \int dy \sqrt{\frac{y}{C-y}} = 2C \int d\phi \sin \phi \cos \phi
  \frac{\sin \phi}{\cos \phi} = 2C \int d \phi \sin^2 \phi = C \left(
    \phi - \frac{1}{2} \sin 2 \phi \right). 
\end{equation} 
Таким образом, получаем, что $x = C_1 + C(\phi - \frac{1}{2} \sin
2\phi)$. Константа $C_1$ определяется из начального условия $x=y=0$
(это соответствует точке $A$). Заметим, что $y=0$ отвечает $\phi=0$,
значит, при $\phi=0$ $x=0$, значит, $C_1=0$. Выпишем ответ:
\begin{equation}
  \label{eq:cycloid-03}
  x = \frac{C}{2} \left( 2\phi - \sin 2\phi \right), \quad y = C
  \sin^2 \phi = \frac{C}{2} \left( 1- \cos 2\phi \right). 
\end{equation}
Переобозначая все константы для удобства, окончательно
\begin{equation}
  \label{eq:cycloid-04}
  x = a \left( \beta - \sin \beta \right), \quad y = a \left( 1 - \cos
  \beta\right). 
\end{equation}
Это параметрическое задание кривой, которая называется
\textbf{циклоидой}. Константа $a$ задаётся конечной точкой $B$. В
нашем случае координаты точки $B$ равны $(L,L)$, что даёт
\begin{equation}
  \label{eq:cycloid-05}
  L = a (\hat{\beta} - \sin \hat{\beta}) = a (1-\cos \hat{\beta}), 
\end{equation}
откуда $\hat{\beta} \approx 2.4$, и $a = L/(1-\cos \hat{\beta})
\approx 0.57 L$. 

Теперь вычислим время, которое необходимо, чтобы по такой кривой
попасть из $A$ в $B$. Мгновенная скорость при движении по кривой равна
\begin{equation}
  \label{eq:cycloid-06}
  v = \frac{ds}{dt} = \frac{\sqrt{(dx)^2 + (dy)^2}}{dt},
\end{equation}
откуда
\begin{equation}
  \label{eq:cycloid-07}
  T = \int \frac{\sqrt{(dx)^2 + (dy)^2}}{v} = \int \frac{\sqrt{(dx)^2
      + (dy)^2}}{\sqrt{2gy}}. 
\end{equation}
Вычислим $dx$ и $dy$, используя соотношения \eqref{eq:cycloid-04}:
\begin{equation}
  \label{eq:cycloid-08}
  dx = a(1 - \cos \beta) d\beta, \quad dy =a \sin \beta d \beta. 
\end{equation}
Подставляя это обратно в интеграл, получим
\begin{equation}
  \label{eq:cycloid-09}
  T = \int\limits_0^{\hat{\beta}} d\beta \frac{a \sqrt{(1-\cos
      \beta)^2 + \sin^2 \beta}}{\sqrt{2g a (1 - \cos \beta)}} =
  \int\limits_0^{\hat{\beta}} d \beta \frac{a\sqrt{2 - 2 \cos
      \beta}}{\sqrt{2g a
      (1 - \cos \beta)}} = \hat{\beta} \sqrt{\frac{a}{g}} \approx 1.82
  \sqrt{\frac{L}{g}}. 
\end{equation}
Здесь интеграл берётся от начальной точки $A$ ($\beta=0$) до конечной
точки $B$ ($\beta = \hat{\beta}$). Заметим, что ответ получился в
самом деле меньше, чем в <<наивном>> приближении прямой (формула
\eqref{eq:linear-time}). 

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-engine:xetex
%%% TeX-PDF-mode: t
%%% End:
