% Игорь Шендерович, ноябрь 2022
\input{../../../main.tex}

\parindent=5mm

\begin{document}

\begin{center}
  \LARGE{\textbf{Теория возмущений и безразмерные параметры}}
\end{center}

\begin{abstract}
  Это записки к лекции по теории возмущений для школьников. Лекция
  была прочитана в ноябре 2022 года в ФМЛ 30 в рамках дня науки. Цель
  этой лекции --- познакомить школьников с идеей теории возмущений на
  простейшем примере. Пример хорош тем, что допускает точное решение
  (это просто квадратное уравнение), поэтому любопытные школьники
  могут проверить, насколько хорошо работает приближённое решение. 
\end{abstract}

\section{Метод последовательных приближений}
\label{sec:successive-approximations}

Рассмотрим простую задачу: хотим измерить глубину сухого колодца, имея
камень и секундомер. Ясно, что для решения нужно кинуть камень в
колодец и замерить время $T$ между бросанием камня и тем, когда мы
услышим звук от его падения. Если обозначить глубину колодца за $H$, а
за $c$ — скорость звука, то получится такое уравнение:

\begin{equation}
  \label{eq:model}
  T = \sqrt{\frac{2H}{g}} + \frac{H}{c}. 
\end{equation}

Решить это уравнение, конечно же, можно (это всего лишь квадратное
уравнение на $H$), но это немного муторно, и к тому же, придётся
извлекать квадратные корни. Попробуем действовать иначе, опираясь на
то, что скорость звука $c$ достаточно велика. Здесь появляется первый
вопрос: по сравнению с чем? Ведь сравнивать можно только величины
одинаковой размерности, а никакой другой характерной скорости у нас в
задаче нет.

Отложим решение этого вопроса на время; вместо этого будем действовать
по-простому и для начала просто выкинем второе слагаемое в уравнении
\eqref{eq:model}. Это даст нам <<нулевое приближение>> для глубины
колодца $h_0$ (пусть для простоты $T=4\unit{с}$):

\begin{equation}
  \label{eq:approx-zero}
  h_0 = \frac{gT^2}{2} = 80 \unit{м}. 
\end{equation}

Ясно, что таким образом мы оценили глубину колодца сверху,
т.е. предъявили верхнюю границу на его глубину. Попробуем понять, на
сколько мы при этом ошиблись и попытаемся сделать приближение чуть
лучше.

Действительно, вычислим, какое время нужно звуку, чтобы преодолеть
расстояние $h_0$ .  Это время равно $h_0/c \approx 0.24\unit{с}$ (я
считаю скорость звука равной $c \approx 340 \unit{м/с}$). Тогда, опять
же, в рамках этого приближения, можно считать, что камень летел время
$t_1 = T - h_0/c \approx 3.76\unit{с}$.  В этом приближении глубина
колодца уже будет равна

\begin{equation}
  \label{eq:approx-first}
  h_1 = \frac{gt_1^2}{2} \approx 70.7 \unit{м}. 
\end{equation}
Очевидно, что это — нижняя граница на глубину колодца. Можно заметить,
что $h_1$ и $h_0$ различаются даже в первой цифре, что говорит о грубости
сделанных оценок. Как можно улучшить это значение?

Сделаем следующую оценку, выполнив аналогичные вычисления. Поскольку
мы знаем глубину чуть точнее, используем это значение и поймём, что
камень на самом деле летел время $t_2 = T-h_1/c \approx 3.8 \unit{с}$, а колодец
при этом имеет глубину

\begin{equation}
  \label{eq:approx-second}
  h_2 = \frac{gt_2^2}{2} \approx 72.2 \unit{м}. 
\end{equation}

Обратим внимание на то, что первая цифра уже совпадает с результатом
предыдущей итерации — значит, мы её знаем достоверно. Итак, мы
«зажали» глубину колодца между 70.7 м и 72.2 м, что уже неплохо. Более
того, понятно, как действовать дальше, если мы хотим повышать
точность.

Фактически мы итерациями решаем уравнение
\begin{equation}
  \label{eq:model-graphics}
  T - \frac{H}{c} = \sqrt{\frac{2H}{g}},
\end{equation}
поэтому будет уместно изобразить все наши итерации графически
(см. рис. \ref{fig:successive-approximations}). 

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      axis lines=middle,
      xticklabels=\empty,
      yticklabels=\empty,
      xlabel={$h,\unit{м}$},
      ylabel={$t, \unit{с}$},
      xlabel style={at=(current axis.right of origin), anchor=west},
      ylabel style={at=(current axis.above origin), anchor=south},
      xmin=0, xmax=10,
      ymin=0, ymax=6
      ]
      \pgfmathsetmacro{\t}{4}; %измеренное время T
      \addplot[name path=rhs, thick, color=blue, domain=0:6, samples=200]
      {2*sqrt(x)};
      \addplot[name path=lhs, thick, color=red, domain=0:10,
      samples=50] {\t-0.25*x};
      \coordinate (t0) at (0,\t);
      \path [name path=I] (t0) -- (6,\t); 
      \path [name intersections={of=I and rhs,by={sol0}}];
      \draw[dashed] (t0) -- (sol0);
      \path [name path=II,draw,dashed] (sol0) --
      ($(0,0)!(sol0)!(6,0)$); 
      \path [name intersections={of=II and lhs,by={t1}}];
      \path [name path=III] (t1) -- ($(0,0)!(t1)!(0,6)$);
      \path [name intersections={of=III and rhs,by={sol1}}];
      \draw[dashed] (t1) -- (sol1);
      \draw[dashed] (sol1) -- ($(0,0)!(sol1)!(6,0)$);
      \draw[dashed] (sol1) -- ($(0,0)!(sol1)!(0,6)$);
      \addlegendentry{\small $\sqrt{2h/g}$};
      \addlegendentry{\small $t-h/c$};
    \end{axis}
    \draw ($(0,0)!(sol0)!(6,0)$) node[below] {$h_0$};
    \draw ($(0,0)!(sol1)!(6,0)$) node[below] {$h_1$};
    \draw ($(0,0)!(sol0)!(0,6)$) node[left] {$T$};
    \draw ($(0,0)!(sol1)!(0,6)$) node[left] {$t_1$};
  \end{tikzpicture}
  \caption{Метод последовательных приближений.}
  \label{fig:successive-approximations}
\end{figure}

Глядя на картинку, можно попробовать сформулировать условия, когда
этот процесс сходится. Остаётся более внятно понять, откуда берётся
эта сходимость. Для этого подойдём к этой задаче немного с другой
стороны.

\section{Теория возмущений}
\label{sec:perturbations}

Чтобы решить эту задачу более чётко и формально, сначала стоит
заметить, что тут есть два безразмерных параметра:
\begin{equation}
  \label{eq:dimensionless-parameters}
  \bar{T} = \frac{gT}{c}, \quad \bar{H} = \frac{H}{gT^2}.
\end{equation}
В этих параметрах наше исходное уравнение \eqref{eq:model}
переписывается так:
\begin{equation}
  \label{eq:dimensionless-model}
  1 = \sqrt{2\bar{H}} + \bar{T}\bar{H}.
\end{equation}

Теперь будем решать задачу в предположении, что параметр $\bar{T}$
малый, т.е. $\bar{T} \ll 1$. Разумеется, это предположение физически
оправдано --- это критерий того, что скорость звука $c$ достаточно
велика (по сравнению с характерной скоростью в задаче $gT$). Тогда
нулевое приближение будет состоять в том, что второе слагаемое в
правой части \eqref{eq:dimensionless-parameters} мы вообще отбросим,
получив, что
\begin{equation}
  \label{eq:perturbations-zero}
  1 = \sqrt{2 \bar{h}_0}, \quad \bar{h}_0 = 1/2. 
\end{equation}

Следующее приближение будет выглядеть так: $\bar{h}_1 = \bar{h}_0 +
\alpha_1 \bar{T}$, где $\alpha_1$~---~неизвестный коэффициент, который
надо найти. Тогда из уравнения \eqref{eq:dimensionless-model} получим,

\begin{equation}
  \label{eq:perturbations-first}
  1 = \sqrt{2\left(\frac{1}{2} + \alpha_1 \bar{T}\right)} + \bar{T} \left(\frac{1}{2} +
  \alpha_1 \bar{T} \right).  
\end{equation}
Поскольку $\bar{T}$ мало, то корень можно разложить, т.е. написать,
что $\sqrt{1+x} \approx 1 + x/2$ при $x \ll 1$. Тогда получим
\begin{equation}
  \label{eq:perturbations-first-add}
  1 = 1 + \alpha_1 \bar{T} + \frac{\bar{T}}{2} + \alpha_1 \bar{T}^2. 
\end{equation}
Поскольку мы сейчас всё делаем в линейном приближении, то
квардатичное слагаемое нужно отбросить, и тогда сразу получится, что
$\alpha_1 = -1/2$. Таким образом, в линейном приближении получаем, что
$\bar{h}_1 = 1/2 - \bar{T}/2$, или, переходя к обычным параметрам,
\begin{equation}
  \label{eq:perturbations-answer}
  h_1 = \frac{gT^2}{2} - \frac{g^2T^3}{2c} \approx 70.6\unit{м}.
\end{equation}
Это ответ в линейном приближении. Понятно, как улучшить его дальше ---
надо написать
\begin{equation}
  \label{eq:perturbations-second}
  \bar{h}_2 = \frac{1}{2} - \frac{\bar{T}}{2} + \alpha_2 \bar{T}^2,
\end{equation}
и решить получившееся уравнение относительно $\alpha_2$ (получится
$\alpha_2 = 5/8$ и $h_2 \approx 72\unit{м}$).

Проводя эту процедуру дальше, можем получить ответ в виде ряда:
\begin{equation}
  \label{eq:perturbations-full}
  \bar{H} = \sum\limits_{n=0}^{\infty} \alpha_n \bar{T}^n,
\end{equation}
а условия на коэффициенты $\alpha_n$ получаются из уравнения
\eqref{eq:dimensionless-model}. Этот способ намного лучше, чем
описанный в предыдущем разделе --- на каждом шаге мы знаем, чем
пренебрегаем и что конкретно мы отбрасываем. Так работает
\textbf{теория возмущений} --- фактически, мы решали уравнение
\eqref{eq:dimensionless-model} имея в виду, что \textbf{возмущение}
$\bar{T}\bar{H}$ мало. 

\section{Замечания}
\label{sec:notes}

Может появиться вопрос --- как процедура из раздела
\ref{sec:successive-approximations} (т.е. итеративное решение
уравнения \eqref{eq:model-graphics}) согласуется с ответом в форме
\eqref{eq:perturbations-full}? Ответить на этот вопрос не так
просто. Дело в том, что в рамках итеративной процедуры сложно
отследить порядок отбрасываемого куска ответа. Например, раскрывая
выражение для $h_1$ (уравнение \eqref{eq:approx-first}), получим

\begin{equation}
  \label{eq:compare-first}
  h_1 = \frac{gT^2}{2} - \frac{g^2T^3}{2c} + \frac{g^3T^4}{8c^2},
\end{equation}
что отличается от \eqref{eq:perturbations-answer} на последнее
слагаемое (которое имеет больший порядок малости). Однако,
оказывается, что это слагаемое ненадёжное. Действительно, уже во
второй итерации (уравнение \eqref{eq:approx-second}) видим
\begin{equation}
  \label{eq:compare-second}
  h_2 = \frac{gT^2}{2} - \frac{g^2T^3}{2c} + \frac{5g^3T^4}{8c^2} + \ldots,
\end{equation}
т.е. коэффициент в третьем слагаемом поменялся (и теперь ответ совпадает
с \eqref{eq:perturbations-second}). В этом и есть проблема метода
последовательных приближений (хотя в этом случае он довольно быстро
сходится и даёт приемлемый по точности ответ). 


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-engine:xetex
%%% TeX-PDF-mode: t
%%% End:
