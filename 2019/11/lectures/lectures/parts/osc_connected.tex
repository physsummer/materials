\section{Колебания связанных систем}

До этого момента мы рассматривали колебания с одной степенью свободы, но в реальности таких систем
достаточно мало.

\begin{definition}
    \textbf{Связанная система}~--- система со многими степенями свободы, между
    которыми возможна передача энергии.
\end{definition}

\begin{figure}[h]
    \centering
    \input{pics/connected_pend.tex}
\end{figure}


Рассмотрим пример такой системы. Пусть это будет два груза соединённых пружиной жёсткости $k$, на
подвесах длины $\ell$.

Будем считать колебания каждого из грузов малыми. Тогда уравнения движения по оси $x$:
\begin{equation}
	\begin{aligned}
        m \ddot{x}_1 = - mg \frac{x_1}{\ell} + k (x_2 - x_1) \\
        m \ddot{x}_2 = - mg \frac{x_2}{\ell} - k (x_2 - x_1),\\
	\end{aligned}
\end{equation}
где $x_{1,2}$~--- координаты грузов по горизонтали (за начало отсчёта принимаем положение равновесия
левого груза). Если сложить и вычесть эти уравнения друг из друга, получится: 
\begin{equation}
    \begin{aligned}
        &\ddot{x}_1 + \ddot{x}_2 = - \frac{g}{\ell} \left( x_1 + x_2 \right)\\
        &\ddot{x}_1 - \ddot{x}_2 = - \frac{g}{\ell} \left( x_1 - x_2 \right) + 2
            \frac{k}{m} \left( x_2 - x_1 \right).
    \end{aligned}
\end{equation}

Видно, что система разделилась и появились две некоторые новые переменные. причем каждое из уравнений
описывает независимое <<колебательное движение>>.
\begin{equation}
    \label{eq:double-syst}
	\begin{aligned}
		\dv[2]{}{t} (x_1 + x_2 ) + \omega_1^2  (x_1 + x_2) = 0\\
		\dv[2]{}{t} (x_2 - x_1 ) + \omega_2^2  (x_2 - x_1) = 0\\
	\end{aligned}
\end{equation}

Частоты этих колебаний равны:
\begin{equation}
	\omega_1 = \sqrt{\frac{g}{\ell}} 
	\qquad
	\omega_2 = \sqrt{\frac{g}{\ell} + 2\frac{k}{m}}
\end{equation}
Теперь можем записать решение (\ref{eq:double-syst})
\begin{equation}
	\begin{aligned}
		&x_2 + x_1 = A \cos \left( \omega_1 t + \varphi_1 \right) \\
		&x_2 - x_1 = B \cos \left( \omega_2 t + \varphi_2 \right)
	\end{aligned}
\end{equation}
Получили решения для колебания сразу двух грузиков с точностью до задания начальных условий.



\subsection{Маятник с быстро колеблющимся подвесом (маятник Капицы)}

Как известно у математического маятника есть два положения равновесия: устойчивое нижнее и неустойчивое
верхнее. Но ситуация существенно меняется, когда точка подвеса начинает совершать быстрые колебания
(частота $\omega$ которых много больше $\omega_0 = \sqrt{\frac{\ell}{g}}$). В таком случае характер
равновесия существенно меняется. В данном разделе мы попробуем найти новые положения равновесия и
определить их устойчивость.


Рассмотрим случай, когда точка подвеса маятника колеблется только по вертикали: $Y = Y_0 \cos \omega
t$. Координаты тела описываются:
\begin{equation}
    \mathbf{r} = Y_0 \cos\omega t \mathbf j + \ell (\sin\varphi \mathbf{i} + \cos\varphi\mathbf{j}),
\end{equation}
где $\mathbf{i}$ и $\mathbf{j}$ единичные вектора по горизонтали и вертикали соответственно. Нас
интересует случай, когда $w >> w_0$, а также $Y_0 << \ell$.

Попробуем записать уравнения движения через момент сил.
\begin{equation}
    m{\ell}^2 \ddot{\varphi} = - mg\ell \sin \varphi + M_{\text{eff}},
\end{equation}
где $M_{eff}$ некоторый эффективный момент который создаётся за счёт вибрации точки подвеса. Попробуем
понять как он выглядит. Перейдём в неинерциальную систему отсчёта подвеса, это добавит ещё одно слагаемое
во второй закон Ньютона. В случае вертикальных колебаний это соответствует периодическому изменению
ускорения свободного падения.
\begin{equation}
    g' = g + Y_0 \omega^2 \cos \omega t 
\end{equation}

Чтобы получить эффективный момент перейдём обратно. Тогда уравнения движения выглядят следующим образом:
\begin{equation}\
    \label{eq:kaptsa-pend-eq}
    m \ell^2 \ddot{\varphi} = -m \ell (g - Y_0 \omega^2 \cos \omega t) \sin \varphi,
\end{equation}

\begin{equation}
    M_{\text{eff}}= mY_0 \omega^2 \ell \cos\omega t \sin\varphi.
\end{equation}

Уравнение (\ref{eq:kaptsa-pend-eq}) достаточно трудно. Для его упрощения вспомним, что нас интересует не
общий случай, а только ситуация быстрых колебаний. Будем считать, что за период колебаний подвеса,
положение маятника не меняется, то есть искать решение в виде:
\begin{equation}
    \label{eq:alpha-beta}
    \varphi(t) = \alpha(t) + \beta(\psi, t),
\end{equation}
где $\psi$~--- фаза колебаний подвеса. Условие про быстроту колебаний нам дает следующий факт:
\begin{equation}
    \label{eq:kapitsa-beta}
    \avg{\beta}_t = 0.
\end{equation}

Запишем уравнение движения при помощи углов $\alpha$ и $\beta$:
\begin{equation}
	m \ell^2 \ddot{\alpha} + m \ell^2 \ddot{\beta} =  \left( -m\ell g + Y_0 \omega^2 m \ell \cos \omega t
    \right) \cdot \left( \sin\alpha \cos\beta + \sin\beta \cos\alpha \right).
\end{equation}

Заметим, что условие про малость амплитуды колебаний подвеса дает нам условие на малость угла
$\beta$. Разложим синус и косинус до первого порядка:
\begin{equation}
	m \ell^2 \ddot{\alpha} + m\ell^2 \ddot{\beta} = \left( -m \ell g + Y_0 \omega^2 m \ell \cos\omega t
    \right) \cdot \left( \sin\alpha + \beta \cos\alpha \right).
\end{equation}
Попробуем усреднить полученное выражение по периоду колебаний подвеса. Следующие соотношения можно
получить пользуясь предположением о быстроте колебаний.

\[
	\begin{aligned}
        \avg{m \ell^2 \ddot{\alpha}}_t = m \ell^2 \ddot{\alpha} \\
        \avg{m \ell^2 \ddot{\beta}}_t = 0 \\
        \avg{m \ell g \sin\alpha}_t = m \ell g \sin\alpha \\
        \avg{m \ell g \beta \cos\alpha}_t = 0 \\
        \avg{Y_0 \omega^2 m \ell \cos\omega t \sin\alpha}_t = 0
	\end{aligned}
\]

Таким образом:

\begin{equation}
	m\ell^2 \ddot{\alpha} = -m \ell g \sin\alpha + Y_0 \omega^2 m \ell \cos\alpha \cdot \avg{\beta \cos\omega t}_t.
\end{equation}

Чтобы решить это уравнение относительно $\alpha$ необходимо научиться считать:
\begin{equation}
    \label{eq:mean-pendulum}
	\avg{\beta \cos \omega t}_t.
\end{equation}

Для подсчета этого выражения вернемся к уравнениям движения (\ref{eq:kaptsa-pend-eq}). В нашей задаче есть
две <<силы>>~--- сила тяжести и колебания подвеса. Причём сила тяжести <<медленная>>. То есть вклад
колебаний подвеса можно заменить на усредненный по периоду <<быстрых>> колебаний подвеса эффективный
момент $m \ell^2 \Upsilon(\alpha)$. Поэтому уравнение (\ref{eq:kaptsa-pend-eq}) можно разбить на два
разных уравнения:
\begin{equation}
	\left\{
      \begin{aligned}
          & m \ell^2 \ddot{\alpha}  = -m g \ell \sin\alpha + m \ell^2 \Upsilon(\alpha) \\
          & m \ell^2 \ddot{\beta} = Y_0 \omega^2 m \ell \sin\alpha \cos\omega t
      \end{aligned}
    \right.
\end{equation}

Запишем эти уравнения в каноническом виде:
\begin{equation}
    \left\{
      \begin{aligned}
          & \ddot{\alpha}  = -\omega_0^2 \sin\alpha + \Upsilon(\alpha)\\
          & \ddot{\beta} = \left( \frac{Y_0}{\ell} \right) \cdot \omega^2 \sin\alpha \cos\omega t
      \end{aligned}
    \right.
\end{equation}

Второе уравнение можно проинтегрировать по времени, чтобы найти зависимость $\beta(t)$:
\begin{equation}
	\beta(t) = - \left( \frac{Y_0}{\ell} \right) \sin\alpha \cos\omega t + At + B.
\end{equation}

Здесь $A$ и $B$~--- константы интегрирования. Для того, чтобы их определить вспомним, что есть условие
(\ref{eq:kapitsa-beta}), исходя из которого обе константы должны быть равны $0$. Таким образом выражение
для осцилляции быстрой составляющей угла:
\begin{equation}
	\beta(t) = -\left( \frac{Y_0}{\ell} \right) \sin\alpha \cos\omega t.
\end{equation}

После того, как найдена эта зависимость можем усреднить (\ref{eq:mean-pendulum}) явно (среднее значение
$\cos^2$ мы уже считали в разделе \ref{sec:energy}):
\begin{equation}
	\avg{\beta \cos\omega t} = - \left( \frac{Y_0}{\ell} \right) \cdot \sin\alpha \avg{\cos^2 \omega t} =
    -\frac{1}{2} \left( \frac{Y_0}{\ell} \right) \sin\alpha.
\end{equation}

Теперь наконец можем написать замкнутое уравнение для $\alpha$.
\begin{equation}
    \label{eq:kapitsa-alpha-eq}
	\ddot{\alpha} = -\omega_0^2 \sin\alpha - \left( \frac{\omega^2 Y_0^2}{2 \ell^2} \right) \sin\alpha
    \cos\alpha
\end{equation}

Вспомним, что мы хотим исследовать положения равновесия маятника. Данные положения определяются условием
$\ddot{\alpha} = 0$, таким образом мы приходим к следующему уравнению:
\begin{equation}
    \label{eq:stable-points}
	\omega_0^2 \sin\alpha = - \left( \frac{\omega^2 Y_0^2}{2 \ell^2} \right) \sin\alpha \cos\alpha
\end{equation}

У этого уравнения есть два уже известных решения, которые соответствуют $\alpha = 0$ и $\alpha = \pi$,
отвечающие за вертикальные положения. Но есть и ещё решение, которые определяются:
\begin{equation}
	\cos\alpha = - 2\left( \frac{\omega_0 \ell}{Y_0 \omega} \right)^2.
\end{equation}

В случае свободного маятника нижнее положение было устойчивым, а верхнее нет. Давайте попробуем понять
что изменилось в случае колеблющегося подвеса. Рассмотрим параметр
$\chi \coloneqq 2\left( \frac{\omega_0 \ell}{Y_0\omega} \right)^2 > 0$ и перепишем уравнение
(\ref{eq:kapitsa-alpha-eq}) в более удобном виде:
\[
	\ddot{\alpha} = - \omega_0^2 \sin\alpha (1 + \frac{1}{\chi} \cos\alpha).
\]

Положение равновесия устойчиво если, при небольшом изменении угла ускорение тела будет направлено против
изменения, таким образом:
\begin{itemize}
    \item $\alpha = 0$, соответствует устойчивому положению, т.к. если рассмотреть малый положительный
        угол, то $\sin \alpha > 0$ и $\ddot{\alpha} < 0$ (аналогично с малый отрицательный);
    \item при $\alpha = \pi$, $\cos \alpha$ можно заменить на $-1$ и уравнение имеет следующий вид:
        $\ddot{\alpha} = \omega_0^2 \sin\alpha (1 - \frac{1}{\chi}$; таким образом если $\alpha = \pi +
        \epsilon$, то знак ускорения будет отрицательным (а следовательно положение устойчивым) тогда и
        только тогда, когда $\chi < 1$ (аналогично $\alpha = \pi - \varepsilon$);
    \item $\alpha = \pm \alpha_0$, где $\cos \alpha_0 = -\chi$; данное решение имеет место быть только в
        случае, когда $\chi < 1$; таким образом если $\alpha = \alpha_0 + \varepsilon$, то $\sin \alpha > 0$
        и $(1 + \frac{1}{\chi} \cos\alpha) > 0$, а следовательно знак ускорения будет положительным (а
        положение неустойчивым) (аналогично $\alpha = - \alpha_0 \pm \varepsilon$ и $\alpha = \alpha_0 -
        \varepsilon$).
\end{itemize}